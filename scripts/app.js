var App = window.App = Ember.Application.create();

App.Router.map(function() {
  this.resource('signup');
});

App.Store = DS.Store.extend({
  revision: 12,
  adapter:'DS.RESTAdapter'
});

App.Auth = Ember.Auth.create({
  signInEndPoint: '/signin',
  signOutEndPoint: '/signout',
  tokenKey: 'auth_token',
  baseUrl: 'http://localhost:9393',
  //tokenIdKey: 'user_id',
  //userModel: 'App.User',
  sessionAdapter: 'cookie',
  modules: [
    'emberData', 'rememberable'
  ],
  rememberable: {
    tokenKey: 'remember_token',
    period: 7,
    autoRecall: true
  }
});
