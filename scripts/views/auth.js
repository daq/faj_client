App.AuthView = Ember.View.extend({
  templateName: 'auth'
});

App.AuthSignInView = Ember.View.extend({
  templateName: 'signin',
  email: null,
  password: null,
  remember: true,
  submit: function(event, view){
    event.preventDefault();
    event.stopPropagation();
    App.Auth.signIn({
      data:{
        email: this.get('email'),
        password: this.get('password'),
        remember: this.get('remember')
      }
    })
  }
});

App.AuthSignOutView = Ember.View.extend({
  templateName: 'signout',
  submit: function(event, view){
    event.preventDefault();
    event.stopPropagation();
    App.Auth.signOut();
  }
});